var request = require('request');
var fileUtils = require('./fileUtils.js');

module.exports = {

	//METODO PARA CREAR LA CONFIGURACION PARA CREAR PAGINAS EN CONFLUENCE
	crearOpciones: function (urlConfluence, auth, bodyData){
		var opciones = {
			   				url: urlConfluence + '/rest/api/content',
			   				method: 'POST',
			   				headers: {
			      						'Authorization': auth,
			      						'X-Atlassian-Token': 'nocheck',
			      						'Accept': 'application/json',
			      						'Content-Type': 'application/json'
			   				},
			   				body: bodyData
        };
		return opciones;
	},

	//METODO PARA CRECOGER DATOS CONFLUENCE
	crearOpcionesGET: function (urlConfluence, auth, titulo, espacio){
		var encoded = encodeURI(urlConfluence + '/rest/api/content?title=' + titulo + '&spaceKey=' + espacio);
		var opciones = {
			   				url: encoded,
			   				method: 'GET',
			   				headers: {
			      						'Authorization': auth,
			      						'Accept': 'application/json',
			   				}
					};

		return opciones;
	},
    //METODO PARA CREAR LABELS
    crearLabels: function (etiquetas) {
        labels = `[`;
        for (var i = 0; i < etiquetas.length; i++) {
            labels += `{
                "prefix": "global",
                "name": "${etiquetas[i]}"
            }`
            if ((i + 1) < etiquetas.length) labels += `,`;
        }
        labels += ` ]`;
        return labels;
    },
	//METODO PARA CREAR BODY DE PAGINA PADRE
	crearBody: function(titulo, espacio, value){
		var body = `{
			"title":"${titulo}",
			"type":"page", 
			"space":{
				"key":"${espacio}"
			},
			"body":{
				"storage":{
							"value":"<p>${value}</p>",
							"representation":"storage"
				}
			}
		}`;

		return body;
	},

	//METODO PARA CREAR BODY DE PAGINA HIJO
	crearBodyHijo: function(titulo, espacio, ancestors, value,labels){
		var body = `{
			"title":"${titulo}",
			"type":"page", 
			"space":{
				"key":"${espacio}"
			},
			"ancestors":[
				{
					"id":"${ancestors}"
				}
			],
			"body":{
				"storage":{
						"value":"<p>${value}</p>",
						"representation":"storage"
				}
			},
            "metadata":{
                "labels": ${labels}
            }
		}`;
		return body;
	},

	//METODO REQUEST CON PROMISE
    requestPROMISE: function (options) {
		return new Promise((resolve, reject) => {
			request(options, (error, response, body) => {
                if (error) reject(new Error(error));
				resolve(body);
			});
		});
	},

	//METODO PROCESAMIENTO BODY QUE DEVUELVE EL ID
	getIDPagina: async function (options) {
		try {
            var body = await module.exports.requestPROMISE(options).catch(error => fileUtils.comentar('ERROR: ' + error));
			var newBody = JSON.parse(body.toString());
            var iD = newBody.id;
			return iD;

		} catch (error) {
			console.error('::ERROR: getIDPagina::');
			console.error(error);
        }

	},

	//METODO PROCESAMIENTO BODY QUE DEVUELVE EL ID
	getIDPaginaPorTitutlo: async function (options) {
		try {

			var body = await module.exports.requestPROMISE(options).catch( error =>  fileUtils.comentar('ERROR: ' + error));

			var newBody = JSON.parse(body.toString());

			var iD 
			if (newBody.results.length != 0) {
				iD = Object.values(newBody.results[0])[0].toString();
			}else{
				iD = -1
			}
			return iD;

		} catch (error) {
			console.error('::ERROR: getIDPaginaPorTitulo::');
			console.error(error);
		}
	}		

};