
module.exports = function (app, addon) {

    // Redirect root path to /atlassian-connect.json,
    // which will be served by atlassian-connect-express.

    //************************************ REQUIRES **********************************//
    //var util = require('util');
    var request = require('request');
    var fs = require('fs');
    var Confluence = require('confluence-api');
    var fileUtils = require('../utils_JC/fileUtils.js');
    var utils = require('../utils_JC/utils.js');
    const { Pool } = require('pg');
    const pool = new Pool({
        connectionString: process.env.DATABASE_URL,
        ssl: true
    });


    //************************************ PARAMETROS DE CONFIGURACI�N ************************//
    var param = JSON.parse(fs.readFileSync('./configPlugin.json', 'utf8'));
    const user = param.user;
    const pass = param.pass
    const auth = 'Basic ' + Buffer.from(user + ':' + pass).toString('base64');
    const urlJira = param.urlJira;
    const urlConfluence = urlJira + '/wiki';
    const espacio = param.espacio;
    const carpetaDocs = param.carpetaDocs;
    var config = {
        username: user,
        password: pass,
        baseUrl: urlConfluence, 
    };
    var confluence = new Confluence(config);
    //********************************************************************************//
    /*app.get('/', (req, res) => {
        res.redirect('/atlassian-connect.json');
    });*/
    app.get('/db', async (req, res) => {
        try {
            const client = await pool.connect()
            const result = await client.query('SELECT * FROM test_table');
            const results = { 'results': (result) ? result.rows : null };
            res.render('pages/db', results);
            client.release();
        } catch (err) {
            console.error(err);
            res.send("Error " + err);
        }
    })

    app.get('/', (req, res) => {
        res.redirect('/atlassian-connect.json');
    });

    app.get('/web-panel-cmdb', addon.authenticate(), (req, res) => {
    try {
        const user = "jira@nortegas.es";
        const pass = "nortegas";
        const auth = 'Basic ' + Buffer.from(user + ':' + pass).toString('base64');
        var request = require('request');
        var options = {
            method: 'GET',
            url: 'https://nortegas-sd-test.atlassian.net/rest/api/2/issue/' + req.param('issueKey'),
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            }
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            //console.log(
            //    'Response: ' + response.statusCode + ' ' + response.statusMessage
            //);
            let json = JSON.parse(body);
            let email = json.fields.reporter.emailAddress;
            //console.log(email);
            email = email.replace("@", "\@");
            var options = {
                method: 'GET',
                url: 'https://nortegas-sd-test.atlassian.net/rest/api/2/search?jql=Email~' + '\"' + email + '\"',
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                }
            };
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                //console.log(
                //    'Response: ' + response.statusCode + ' ' + response.statusMessage
                //);
                try {
                    let json2 = JSON.parse(body)
                    var arr = json2.issues;
                    let f = JSON.stringify(arr);
                    f = f.slice(1);
                    f = f.slice(0, f.length - 1);
                    json2 = JSON.parse(f);
                    // Rendering a template is easy; the render method takes two params:
                    // name of template and a json object to pass the context in.
                    res.render('documents', {
                        title: 'Equipo del usuario',
                        idEquipo: json2.fields.summary,
                        idIssue: json2.key,
                        issueType: json2.fields.customfield_10156.value,
                        status: json2.fields.status.name,
                        marca: json2.fields.customfield_10140.value,
                        procesador: json2.fields.customfield_10147.value,
                        so: json2.fields.customfield_10207.value
                    });
                } catch (error) {
                    res.render('documents_error', {
                    });
                }
            });           
        });
    } catch (error) {
            res.render('documents_error', {
            }); 
      }
    });

    app.post('/confluence-integration', addon.authenticate(), async (req, res) => {
        try {

            /*
            Recibimos la llamada de JIRA lanzando mediante WEBHOOK desde la transicion en la cual se subira el documento
            Llega la request, recogemos el body de la ISSUE
            Inicializamos las variables necesarias que utilizaremos para trabajar con ellas
            */
            const issueJSON = req.body;
            const issuekey = issueJSON.issue.key;
            const issuesummary = issueJSON.issue.fields.summary;
            const issuedesc = issueJSON.issue.fields.description;
            const nEstado = issueJSON.issue.fields.status.id;
            //Forma de coger elementos cuando devuelve un OBJECT (hay que transformarlo a STRING)
            //const aplicacion = Object.values(issueJSON.issue.fields.customfield_10201)[1];
            const aplicacionPaginaApp = issueJSON.issue.fields.customfield_10201.value;
            let issuePage = issuekey + ': ' + issuesummary;
            let idAncestor = '';
            let idAncestor11 = '';
            let idAncestor2 = '';
            let idAncestor3 = '';
            let first = 0;
            let iniciales = '';
            fileUtils.comentar('/******** JIRA issue ==> ' + issuekey);

            function unhyphenate(str) {
                return str.replace(/(\w)(-)(\w)/g, '$1 $3');
            }

            function string_to_array (str) {
                return str.trim().split(" ");
            };

            function fase(E) {
                switch (E) {
                    case '10053':
                        // code block
                        return "Requerimientos"
                        break;
                    case '10036':
                        // code block
                        return 'Estimaci\xF3n';
                        break;
                    case '10037':
                        // code block
                        return 'Planificaci\xF3n';
                        break;
                    case '10057':
                        // code block
                        return 'Dise\xF1o Funcional';
                        break;
                    case '10039':
                        // code block
                        return 'Dise\xF1o T\xE9cnico';
                        break;
                    case '10059':
                        // code block
                        return 'Transporte Integraci\xF3n';
                        break;
                    case '10062':
                        // code block
                        return 'Pruebas';
                        break;
                    case '10094':
                        // code block
                        return 'Transporte Producci\xF3n';
                        break;
                    default:
                        // code block
                        fileUtils.comentar('NO ENCUENTRA ESTADO');
                        return '';
                }

            }
	  	    /*
	  	    Preparamos la request al API de JIRA con la que cogeremos los datos del ATTACHMENT 
	  	    La Auth se hara mediante la creacion de un token
		    */
            var options = {
                method: 'GET',
                url: urlJira + '/rest/api/2/issue/' + issuekey,
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                }
            };

            request(options, function (error, response, body) {

                fileUtils.comentar('PROCESAR ATTACHMENT');

                if (error) throw new Error(error);



	  		/*
	  		Procesamos el texto para coger la etiqueta del attach solamente
	  		Despues lo convertiremos en JSON
	  		Buscaremos entre todos los attach los que tengan el nombre de documentacion tecnica
	  		*/

                var ini = body.match(/attachment/i);
                var fin = body.match(/summary/i);
                var attachmentJSON = '{' + body.substring(ini.index - 1, fin.index - 2) + '}';
                var newBody = JSON.parse(attachmentJSON);

	 		/*
	 		Crearemos un array en el cual a�adriemos los documentos tecnicos en concreto,
	 		los cuales procesaremos posteriormente.
	 		*/

                var attachArray = [];

                for (var i = 0; i < newBody.attachment.length; i++) {
                    var attachs = newBody.attachment[i];
                    attachArray.push(attachs);
                }

                var rutaArray = [];

                /*
                Recogemos todos los attachment
                */

                for (var j = 0; j < attachArray.length; j++) {
                    var elFichero = attachArray[j];
                    options = {
                        method: 'GET',
                        url: elFichero.content,
                        headers: {
                            'Authorization': auth,
                            'Content-Type': 'application/json'
                        }
                    };

                    var rutaFichero = carpetaDocs + elFichero.filename;

                    rutaArray.push(rutaFichero);

                    request(options, function (error, response, body) {
                        if (error) throw new Error(error);
                    }).pipe(fs.createWriteStream(rutaFichero));

                    options = {
                        method: 'DELETE',
                        url: urlJira + '/rest/api/3/attachment/' + elFichero.id,
                        headers: {
                            'Authorization': auth,
                            'Content-Type': 'application/json'
                        }
                    };

                    request(options, function (error, response, body) {
                        if (error) throw new Error(error);
                    });

                }

            });

            //FUNCION QUE CREAR LOS ATTACHMENT
            function crearAttachment(espacio, idPageEstado, rutaF) {
                return new Promise((resolve, reject) => {
                    confluence.createAttachment(espacio, idPageEstado, rutaF, function (error, data) {
                        //update attachment properties API
                        if (error) reject(new Error(error));
                        resolve(data);

                    })
                });
            };

            //FUNCION QUE CREAR EL ARRAY DE ADJUNTOS QUE VA EN CADA PAGINA DE ESTADO
            async function adjuntos(idPageEstado) {

                var subRutaArray = [];

                fs.readdirSync(carpetaDocs).forEach(file => {
                    var rutaFi = carpetaDocs + file;
                    subRutaArray.push(rutaFi);
                });

 

                for (var m = 0; m < subRutaArray.length; m++) {
                    var ruta = subRutaArray[m];
                    await crearAttachment(espacio, idPageEstado, ruta).catch(error => fileUtils.comentar('::ERROR: adjuntos::' + error));
                    fs.unlinkSync(ruta);
                }

            }

            //FUNCION QUE CREA LA FASE Y ADJUNTA LOS DOCUMENTOS
            async function crearPaginaPhase() {
                try {

                    var nombreEstado = fase(nEstado)

                    fileUtils.comentar('CREANDO P�GINA FASE:' + nombreEstado);

                    var titulo = nombreEstado + ' ' + issuekey;
                    var labels = '';

                    var value = "<ac:structured-macro ac:name=\\\"attachments\\\"><ac:parameter ac:name=\\\"sortBy\\\">name</ac:parameter><ac:parameter ac:name=\\\"sortOrder\\\">ascending</ac:parameter></ac:structured-macro>";
                    // Crear Pagina de Fase
                    var bodyData = "";

                    /***************************  CREAMOS ETIQUETAS ******************************************/
                    var etiquetasApp;
                    var etiquetasSum;
                    var etiquetaKey;
                    var etiquetaNApp;
                    var etiquetas;
                    var aplicacion = unhyphenate(aplicacionPaginaApp);
                    var resumen = unhyphenate(issuesummary);

                    etiquetaNApp = string_to_array(iniciales);

                    etiquetas = etiquetaNApp.concat(string_to_array(issuekey));
                    etiquetas = etiquetas.concat(string_to_array(nombreEstado));
                    etiquetas = etiquetas.concat(string_to_array(resumen));
                    etiquetas = etiquetas.concat(string_to_array(aplicacion));
                    etiquetas = etiquetas.concat(string_to_array('evolutivo'));

                    /*******************************************************************************************/

                    var labels = utils.crearLabels(etiquetas);

                    if (first == 1) {
                        bodyData = utils.crearBodyHijo(titulo, espacio, idAncestor3, value, labels);
                        first = 0;
                    }
                    else {
                        bodyData = utils.crearBodyHijo(titulo, espacio, idAncestor2, value, labels);
                    }
                    options = utils.crearOpciones(urlConfluence, auth, bodyData);
                    var idPageEstado;
                    await utils.getIDPagina(options).then(iD => idPageEstado = iD).catch(error => fileUtils.comentar('::ERROR: CrearLabels::' + error));

                    await adjuntos(idPageEstado);
                    //}
                } catch (error) {
                    console.log("::ERROR crearPaginaPhase::" + error);
                }
            }

            //FUNCION QUE CREA LAS PAGINAS ISSUE
            async function crearPaginaIssue() {
                try {

                    fileUtils.comentar('CREANDO P�GINA ISSUE');

                    var value = escape(issuedesc);

                    //AQUI HAY UN PROBLEMA CON LOS CARACTERES ESPECIALES
                    value += '</p><br></br><p><ac:structured-macro ac:name=\\\"jira\\\" ac:schema-version=\\\"1\\\"><ac:parameter ac:name=\\\"key\\\">' + issuekey + '</ac:parameter></ac:structured-macro></p>';
                    value += '<br></br><p><ac:structured-macro ac:name=\\\"info\\\"><ac:parameter ac:name=\\\"icon\\\">true</ac:parameter><ac:rich-text-body><p>El evolutivo contiene la siguiente documentaci\xF3n organizada en las diferentes fases de aportaci\xF3n de documentaci\xF3n del flujo de trabajo.</p></ac:rich-text-body></ac:structured-macro></p>';
                    value += '<p><ac:structured-macro ac:name=\\\"pagetree\\\"><ac:parameter ac:name=\\\"root\\\"><ac:link><ri:page ri:content-title=\\\"' + issuePage + '\\\"/></ac:link></ac:parameter><ac:parameter ac:name=\\\"startDepth\\\">1</ac:parameter></ac:structured-macro>';


                    /***************************  CREAMOS ETIQUETAS ******************************************/
                    var etiquetasApp;
                    var etiquetasSum;
                    var etiquetaKey;
                    var etiquetaNApp;
                    var etiquetas;
                    var aplicacion = unhyphenate(aplicacionPaginaApp);
                    var resumen = unhyphenate(issuesummary);

                    etiquetaNApp = string_to_array(iniciales);

                    etiquetas = etiquetaNApp.concat(string_to_array(issuekey));
                    etiquetas = etiquetas.concat(string_to_array(resumen));
                    etiquetas = etiquetas.concat(string_to_array(aplicacion));
                    etiquetas = etiquetas.concat(string_to_array('evolutivo'));

                    /*******************************************************************************************/

                    var labels = utils.crearLabels(etiquetas);

                    // Crear Pagina de Issue
                    var bodyData = utils.crearBodyHijo(issuePage, espacio, idAncestor11, value, labels);
                    options = utils.crearOpciones(urlConfluence, auth, bodyData);
                    await utils.getIDPagina(options).then(iD => idAncestor3 = iD).catch(error => fileUtils.comentar('ERROR: ' + error));
                    first = 1;
                    await crearPaginaPhase();
                } catch (error) {
                    console.log("::ERROR crearPaginaIssue::" + error);
                }
            }

            //FUNCION QUE CREA LAS PAGINAS APP
            async function crearPaginaApp() {
                try {
                    fileUtils.comentar('CREANDO P�GINA APP');
                    var value = '';
                    first = 1;
                    //Crear la pagina de aplicaci�n
                    var bodyData = utils.crearBody(aplicacionPaginaApp, espacio, value);
                    options = utils.crearOpciones(urlConfluence, auth, bodyData);
                    await utils.getIDPagina(options).then(iD => idAncestor = iD).catch(error => fileUtils.comentar('ERROR: ' + error));

                    //Crear la pagina de de Gestion de evolutivos
                    value += '<br></br><p><ac:structured-macro ac:name=\\\"info\\\"><ac:parameter ac:name=\\\"icon\\\">true</ac:parameter><ac:rich-text-body><p>Sobre esta aplicaci�n se han creado los siguientes tickets con documentaci�n organizada en las diferentes fases de aportaci�n de documentaci�n del flujo de trabajo.</p></ac:rich-text-body></ac:structured-macro></p>';
                    value += '<p><ac:structured-macro ac:name=\\\"pagetree\\\"><ac:parameter ac:name=\\\"root\\\"><ac:link><ri:page ri:content-title=\\\"' + titulo + '\\\"/></ac:link></ac:parameter><ac:parameter ac:name=\\\"startDepth\\\">1</ac:parameter></ac:structured-macro>';
                    var labels = utils.crearLabels(aplicacionPaginaApp, '', '');
                    bodyData = utils.crearBodyHijo(titulo, espacio, idAncestor, value, labels);
                    options = utils.crearOpciones(urlConfluence, auth, bodyData);
                    await utils.getIDPagina(options).then(iD => idAncestor11 = iD).catch(error => fileUtils.comentar('ERROR: ' + error));

                    await crearPaginaIssue();
                } catch (error) {
                    console.log("::ERROR crearPaginaApp::" + error);
                }
            }

            //FUNCION INICIAL UNA VEZ RECOGIDOS LOS ATTACHMENT. INVOCAR A CONFLUENCE Y REALIZAR PROCESO
            async function sendToConfluence() {
                try {
                    options = utils.crearOpcionesGET(urlConfluence, auth, aplicacionPaginaApp, espacio);
                    var phase = fase(nEstado) + " " + issuekey;
                    await utils.getIDPaginaPorTitutlo(options).then(iD => idAncestor = iD).catch(error => fileUtils.comentar('ERROR: ' + error));

                    if (idAncestor != -1) { //P�gina App existe         

                        options = utils.crearOpcionesGET(urlConfluence, auth, titulo, espacio);
                        await utils.getIDPaginaPorTitutlo(options).then(iD => idAncestor11 = iD).catch(error => fileUtils.comentar('ERROR: ' + error));

                        options = utils.crearOpcionesGET(urlConfluence, auth, issuePage, espacio);
                        await utils.getIDPaginaPorTitutlo(options).then(iD => idAncestor2 = iD).catch(error => fileUtils.comentar('ERROR: ' + error));

                        if (idAncestor2 != -1) { //P�gina Issue existe
                            options = utils.crearOpcionesGET(urlConfluence, auth, phase, espacio);
                            await utils.getIDPaginaPorTitutlo(options).then(iD => idAncestor3 = iD).catch(error => fileUtils.comentar('ERROR: ' + error));
                            if (idAncestor3 != -1) { //P�gina Phase existe
                                // No creas p�ginas
                                fileUtils.comentar('NO SE CREAN P�GINAS. SE ADJUNTAN DOCUMENTOS');
                                adjuntos(idAncestor3);
                            } else {
                                // Crear Fase
                                fileUtils.comentar('CREAR FASE');
                                await crearPaginaPhase();
                            }
                        } else {
                            // Crear Issue y Fase
                            fileUtils.comentar('CREAR ISSUE Y FASE');
                            await crearPaginaIssue();
                        }
                    } else {
                        // Crear App, Issue y Fase
                        fileUtils.comentar('CREAR APP, ISSUE Y FASE');
                        await crearPaginaApp();
                    }

                } catch (error) {
                    console.log("::ERROR comprobarPadres::" + error);
                }

            }

            //############################################ MAIN ###################################################
            /*
            Para poder crear la pagina que almacenara los attachment es necesario encontrar el ID del PADRE de la cual
            va a colgar.
            Si dentro del espacio ya hay una pagina de esa aplicacion no hacemos nada, pero si no existe la pagina la crearemos.
            */
            fileUtils.comentar('COMIENZO PROCESO');
            const pGE = 'Evolutivos';

            var titulo = '';

            var fileJSON = JSON.parse(fs.readFileSync('./utils_json/aplicaciones.json', 'utf8'));

            var salir = false;
            var i = 0;

            while (i < fileJSON.aplicaciones.length && salir === false) {
                if (fileJSON.aplicaciones[i].aplicacion == aplicacionPaginaApp) {
                    titulo = pGE + ' ' + fileJSON.aplicaciones[i].iniciales;
                    iniciales = fileJSON.aplicaciones[i].iniciales;
                }
                i++;
            }

            await sendToConfluence();

            res.sendStatus(200);

            fileUtils.comentar('FIN DE PROCESO');
           //###################################################################################################

        } catch (error) {
            console.log("::ERROR app.post::"+error);
        }
    });

    app.get('/view', addon.authenticate(), function (req, res) {
        res.render('workflow/view', {
            id: req.param('id')
        });
    });

    app.get('/edit', addon.authenticate(), function (req, res) {
        res.render('workflow/edit', {
            id: req.param('id')
        });
    });

    app.get('/create', function (req, res) {
        res.render('workflow/create', {

        });
    });

    // Add additional route handlers here...
}
