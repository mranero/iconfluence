AP.require(["jira"], function (jira) {
    jira.WorkflowConfiguration.onSave(function () {
        // Display a nice green flag using the Flags JavaScript API.
        var flag = AP.flag.create({
            title: 'Successfully created a flag.',
            body: 'This is a flag.',
            type: 'success',
            actions: {
                'actionkey': 'Click me'
            }
        });
        console.log(arguments);
        return "";
    });
    jira.WorkflowConfiguration.onSaveValidation(function () {

        return true;
    });
});
